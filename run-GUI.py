﻿# -*- coding: utf-8 -*-
# Author Name: Mesut Güneş
# Author Email: gunesmes@gmail.com
# Author Github username: gunesmes

import os, sys, time
sys.path.append("src")

from subTranslater import SubsTranslater


def translater(argv):

    try:

        print("####################################################################################")
        print("                         Hello And Welcome To Shabi Translate                       ")
        print("####################################################################################")
        # set file_dir = "D:/..." the path of files to be converted
        # file_dir = "/Users/mesutgunes/Projects/subtitle_translator"
        temp = input("\nPlease Enter Dir Path (Default current): ")
        if temp == "":
            file_dir = "."
        else:
            file_dir = temp

        # max length of line on tv screen
        # if last word left alone, it's added to the previous line
        temp = input("Please Enter Line Max Length (Default 40): ")
        if temp == "":
            max_length = 40
        else:
            max_length = int(temp)

        # Google can only be choosen as translator. Later, Windows and Yandex can be added.
        # translator = args[2]
        translator = "yandex"

        # set languages you want to translate
        temp = input("Please Enter Source Language (Default en): ")
        if temp == "":
            source_language = "en"
        else:
            source_language = temp
        temp = input("Please Enter Target Language: (Default he)")
        if temp == "":
            target_language = "he"
        else:
            target_language = temp
    except IndexError:
        print("arguments error!")
        sys.exit(2)

    # set directory
    os.chdir(file_dir)
    path = os.listdir(file_dir)

    s = SubsTranslater()

    # get the .srt files from the dir
    files = list()
    for item in path:
        if item.rfind(".srt") != -1:
            files.append(item)
            continue

    for i in range(len(files)):
        subFile = os.path.dirname(os.path.abspath(files[i])) +"/" + files[i]
        
        """
        this function translate a subtitle file from original language to desired  language
        
        fileName        : names of subtitles 
        target_language : language you want to translate to
        source_language : the language of the subtitle
        translator      : Google (later Yandex, Microsoft)
        max_length      : max length of line on tv screen

        """
        
        # s.translate_substitle(fileName, target_language, source_language, translator, max_length)    
        s.translate_substitle(subFile, source_language, target_language, translator, max_length)


if __name__ == "__main__":
    translater(sys.argv[1:])
    print("\nFinished Translating......")
    time.sleep(5)

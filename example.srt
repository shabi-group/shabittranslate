﻿1
00:00:06,000 --> 00:00:12,074
Play one of the best new FPS shooters,
search Steam for PROJECT WARLOCK

2
00:01:53,739 --> 00:01:55,740
(Bells chiming, birds singing)

3
00:02:04,416 --> 00:02:06,417
(Horse whinnies)

4
00:02:07,753 --> 00:02:09,754
(Men shouting)

5
00:02:21,683 --> 00:02:24,143
(Ned) Does Ser Hugh have
any family in the capital?

6
00:02:24,269 --> 00:02:25,770
No.

7
00:02:27,272 --> 00:02:29,816
I stood vigil
for him myself last night.

8
00:02:30,818 --> 00:02:32,819
He had no one else.

9
00:02:34,029 --> 00:02:36,781
He'd never worn this armor before.

10
00:02:36,907 --> 00:02:40,827
Bad luck for him -
going against the Mountain.

11
00:02:40,953 --> 00:02:43,162
Who determines the draw?

12
00:02:43,288 --> 00:02:45,414
All the knights draw straws, Lord Stark.

13
00:02:45,541 --> 00:02:47,333
Aye.

14
00:02:47,459 --> 00:02:49,627
But who holds the straws?

15
00:02:51,547 --> 00:02:53,881
You've done good work, Sisters.

16
00:02:58,679 --> 00:03:00,596
Life is strange.

17
00:03:00,722 --> 00:03:04,141
Not so many years ago
we fought as enemies at the Trident.

18
00:03:04,268 --> 00:03:07,186
I'm glad we never met
on the field, Ser Barristan.

19
00:03:07,312 --> 00:03:10,523
As is my wife. I don't think
the widow's life would suit her.

20
00:03:10,649 --> 00:03:12,859
(Chuckles) You're too modest.

21
00:03:12,985 --> 00:03:15,486
I've seen you cut down
a dozen great knights.

22
00:03:15,612 --> 00:03:19,156
My father once told me
you were the best he'd ever seen.

23
00:03:19,283 --> 00:03:22,201
I never knew the man to be wrong
about matters of combat.

24
00:03:22,327 --> 00:03:24,328
He was a fine man, your father.

25
00:03:25,289 --> 00:03:27,540
What the Mad King
did to him was a terrible crime.

26
00:03:27,666 --> 00:03:29,250
And that lad -

27
00:03:29,376 --> 00:03:31,377
he was a squire
until a few months ago.

28
00:03:31,503 --> 00:03:34,505
How could he afford a new suit of armor?

29
00:03:34,631 --> 00:03:36,966
Perhaps Lord Arryn
left him some money?

30
00:03:38,677 --> 00:03:42,430
- I hear the king wants to joust today.
- Yes, that will never happen.

31
00:03:42,556 --> 00:03:45,266
(Chuckles) Robert tends
to do what he wants.

32
00:03:45,392 --> 00:03:47,894
If the king got what
he wanted all the time,

33
00:03:48,020 --> 00:03:50,521
he'd still be fighting
a damned rebellion.

34
00:03:55,402 --> 00:03:58,571
It's made too small, Your Grace.
It won't go.

35
00:03:58,697 --> 00:04:01,407
Your mother was
a dumb whore with a fat arse.

36
00:04:01,533 --> 00:04:03,534
Did you know that?

37
00:04:05,621 --> 00:04:09,081
Look at this idiot!
One ball and no brains.

38
00:04:09,207 --> 00:04:12,001
He can't even put
a man's armor on him properly.

39
00:04:12,127 --> 00:04:14,962
- You're too fat for your armor.
- Fat?

40
00:04:15,964 --> 00:04:17,548
Fat, is it?

41
00:04:17,674 --> 00:04:20,051
Is that how you speak to your king?

42
00:04:22,679 --> 00:04:25,097
(Laughing)

43
00:04:27,267 --> 00:04:29,060
Oh, it's funny, is it?

44
00:04:31,563 --> 00:04:34,065
- No, Your Grace.
- No?

45
00:04:34,191 --> 00:04:36,525
You don't like the Hand's joke?

46
00:04:37,235 --> 00:04:38,945
You're torturing the poor boy.

47
00:04:39,071 --> 00:04:41,739
You heard the Hand.
The king's too fat for his armor.

48
00:04:41,865 --> 00:04:44,784
Go find the breastplate
stretcher - now!

49
00:04:46,411 --> 00:04:50,122
- (Laughs)
- "The breastplate stretcher"?

50
00:04:50,248 --> 00:04:52,375
How long before he figures it out?

51
00:04:52,501 --> 00:04:55,628
- Maybe you should have one invented.
- All right, all right.

52
00:04:55,754 --> 00:04:58,714
You watch me out there.
I still know how to point a lance.

53
00:04:58,840 --> 00:05:01,467
You've no business jousting.
Leave that for the young men.

54
00:05:01,593 --> 00:05:04,929
Why, because I'm king?
Piss on that. I want to hit somebody!

55
00:05:05,055 --> 00:05:07,181
- And who's going to hit you back?
- Anybody who can.

56
00:05:07,307 --> 00:05:10,184
- And the last man in his saddle...
- Will be you.

57
00:05:10,310 --> 00:05:14,730
There's not a man in the Seven Kingdoms
would risk hurting you.

58
00:05:14,856 --> 00:05:17,149
Are you telling me
those cowards would let me win?

59
00:05:17,275 --> 00:05:18,943
Aye.

60
00:05:24,908 --> 00:05:26,784
- Drink.
- I'm not thirsty.

61
00:05:26,910 --> 00:05:29,245
Drink. Your king commands it.

62
00:05:33,250 --> 00:05:35,042
Gods,

63
00:05:35,168 --> 00:05:37,628
too fat for my armor.

64
00:05:38,630 --> 00:05:41,173
Your squire -
a Lannister boy?

65
00:05:41,299 --> 00:05:43,759
Mm. A bloody idiot,

66
00:05:43,885 --> 00:05:45,845
but Cersei insisted.

67
00:05:46,847 --> 00:05:49,348
I have Jon Arryn to thank for her.

68
00:05:49,474 --> 00:05:52,351
"Cersei Lannister will make
a good match," he told me.

69
00:05:52,477 --> 00:05:55,146
"You'll need her father on your side."

70
00:05:57,107 --> 00:06:01,110
I thought being king
meant I could do whatever I wanted.

71
00:06:04,072 --> 00:06:06,240
Enough of this.
Let's go watch 'em ride.

72
00:06:06,366 --> 00:06:09,035
At least I can smell
someone else's blood.

73
00:06:09,161 --> 00:06:11,162
- Robert.
- What?

74
00:06:11,872 --> 00:06:14,874
Oh. (Laughs)

75
00:06:15,000 --> 00:06:17,585
An inspiring sight for the people, eh?

76
00:06:17,711 --> 00:06:20,546
Come, bow before your king!
Bow, you shits!

77
00:06:20,672 --> 00:06:22,673
(Ned chuckles)

78
00:06:24,134 --> 00:06:25,885
(Cheering)

79
00:06:34,644 --> 00:06:37,563
- Where's Arya?
- At her dancing lessons.

80
00:06:40,609 --> 00:06:43,069
The Knight of the Flowers.

81
00:06:49,159 --> 00:06:51,160
Thank you, Ser Loras.

82
00:06:59,878 --> 00:07:03,089
(Horse neighs)

83
00:07:09,888 --> 00:07:11,514
(Crowd jeering)

84
00:07:12,224 --> 00:07:16,143
(Horse grunting and neighing)

85
00:07:22,192 --> 00:07:24,944
Don't let Ser Gregor hurt him.

86
00:07:25,070 --> 00:07:27,696
- Hey.
- I can't watch.

87
00:07:30,575 --> 00:07:32,785
A hundred gold dragons on the Mountain.
